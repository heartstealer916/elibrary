<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\EbookController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PageController::class , 'welcome']);

Auth::routes();

Route::get('admin/home', [HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');
Route::get('admin/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard')->middleware('is_admin');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/profile/{id}' , [ProfileController::class , 'index']);

Route::resource('ebooks' , EbookController::class);
Route::get('/members' , [MemberController:: class , 'index']);
Route::post('/members' , [MemberController:: class , 'store'])->name('members.store');
Route::get('/members/create' , [MemberController:: class , 'create'])->name('members.create');
Route::delete('/members/{id}' , [MemberController:: class , 'destroy'])->name('members.destroy');
Route::get('/members/{id}' , [MemberController:: class , 'show'])->name('members.show');

Route::get('/ebook', [PageController::class , 'ebook']);
Route::get('/contact', [PageController::class , 'contact']);


// Route::get('/admin/profile/{id}' , [ProfileController::class , 'index']);

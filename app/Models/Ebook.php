<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ebook extends Model
{
    use HasFactory;

    protected $table = 'ebooks';
    public $primaryKey = 'id';
    public $foreignKey = 'cat_id';
    public $timeStamps = true; 
    
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'author',
        'notes',
        'pic',
        'pdf',
        'cat_id',
    ];
    public function catagory()
    {
        return $this->hasOne(Catagory::class ,'cat_id');
    }
}


<?php

namespace App\Http\Controllers;

use App\Models\User;

class ProfileController extends Controller
{
    public function index($id)
    {
        $info = User::findOrFail($id);
        return view('profile.index')->with('info',$info);
    }
}

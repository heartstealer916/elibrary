<?php

namespace App\Http\Controllers;

use App\Models\Ebook;

class PageController extends Controller
{
    public function ebook()
    {
        return view('ebook');
    }

    public function contact()
    {
       
        return view('contact');
    }

    public function welcome()
    {
        $ebooks = Ebook::orderBy('created_at')->get();
        return view('welcome',['ebooks'=>$ebooks]);
    }
}
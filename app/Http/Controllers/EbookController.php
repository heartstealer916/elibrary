<?php

namespace App\Http\Controllers;

use App\Models\Ebook;
use Illuminate\Http\Request;
class EbookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ebooks = Ebook::all();
        return view('ebooks.index', ['ebooks'=> $ebooks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ebooks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request ,[
        'name' => 'required',
        'author' => 'required',
        'notes' => 'required|max:100',
        'pic' => 'required|max:2000|image|mimes:jpeg,png,jpg,gif,svg',
        'pdf' => 'required|max:15000|mimes:pdf',

        ],[
            'name' => 'Enter valid Ebook Name',
            'author' => 'Enter valid Author name',
            'notes' => ['Enter some notes','Max contains 100 characters'],
            'pic' => ['Must upload an image','Cover picture size no more than 2 MB','Only images can be uploaded'],
            'pdf' => ['Must upload an ebook pdf file ','Cover picture size no more than 15 MB','Only pdf files can be uploaded'],
        ]);
        $ebook = new Ebook;
        if($request->file('pic') && $request->file('pdf'))
        {
            $cover = $request->file('pic');
            $doc = $request->file('pdf');

            $pic = "covers/".$cover->getClientOriginalName();
            $coverName = $cover->getClientOriginalName();
            $pdf = "docs/".$doc->getClientOriginalName();
            $docName = $doc->getClientOriginalName();
            $cover->move('covers/',$coverName);
            $doc->move('docs/',$docName);
            $ebook->pic = $pic;
            $ebook->pdf = $pdf;
        }
        $ebook->name = $request->name;
        $ebook->author = $request->author;
        $ebook->notes = $request->notes;
        $ebook->save();
        // return $request->file('pdf')->getSize();
        return redirect(route('ebooks.index'));
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ebook = Ebook::find($id);
        return view('ebooks.show' , ['ebook' => $ebook]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ebook::find($id)->delete();

        return back();
    }
}

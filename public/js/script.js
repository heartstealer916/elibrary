$(function ()
{

    $(".profile-btn").on("click", function ()
    {
        $(".profile-info").addClass("hidden");
        $(".orders-info").removeClass("hidden");
    });
    $(".orders-btn").on("click", function ()
    {
        $(".orders-info").addClass("hidden");
        $(".profile-info").removeClass("hidden");
    });

    $(".delete-ebook-form,.delete-member-form").on('submit', function (e)
    {
        var form = this;
        e.preventDefault();
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this one!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((isConfirm) =>
            {
                if (isConfirm)
                {

                    swal("Item has been deleted!", {
                        icon: "success",
                    })
                        .then(function ()
                        {
                            form.submit();
                        });
                } else
                {
                    swal("Item is safe!");
                }
            });
    });

    setTimeout(() =>
    {
        $(".alert").hide();
    }, 4000);



});
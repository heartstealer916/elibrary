@extends('dashboard.master')
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Members</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Members</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid d-flex flex-column p-1 border border-rounded bg-white">
    <div class="add-member mt-3 mx-3">
        <a href="{{ route('members.create')}}">
            <button>
                <i class="fas fa-plus"></i>
                Add Member
            </button>
        </a>
    </div>

    <div class="members-table p-3 w-75">
        <table class="table table-bordered">
            <thead>
                <tr style="font-size: 16px">
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Staues</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>



            <tbody>
                @foreach ($members as $key=>$member)
                <tr>
                    <th scope="row">{{++$key}}</th>
                    <td>{{$member->name}}</td>
                    <td>{{$member->email}}</td>
                    <td>{{__('Member')}}</td>
                    <td>
                        <div class="action-btns d-flex flex-row">
                            <a href="{{route('members.show' , $member->id) }}"><button class="btn btn-success m-1"
                                    data-toggle="tooltip" data-placement="top" title="view" tabindex="0"><i
                                        class="fas fa-eye"></i></button></a>
                            <form class="delete-member-form" action="{{route('members.destroy', $member->id)}}"
                                method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger m-1" type="submit" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="delete" tabindex="0">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
                @endforeach
                <tr style="font-size: 16px">
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Staues</th>
                    <th scope="col">Actions</th>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection
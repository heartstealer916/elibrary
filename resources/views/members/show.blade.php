@extends('dashboard.master')
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Profile</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Members</a></li>
                    <li class="breadcrumb-item active">Show Member details</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container d-flex flex-row p-5 border border-rounded bg-white w-50 ml-4">

    <div class="details m-auto">
        <div class="name">
            <p>
                <h4>Name: </h4>{{$member->name}}
            </p>
        </div>
        <div class="email">
            <p>
                <h4>Email: </h4>{{$member->email}}
            </p>
        </div>
        <div class="status my-1">
            <p>
                <h4>Status:</h4>
                @if ($member->is_admin == 1)
                <p>Admin</p>
                @else
                <p>Member</p>
                @endif
            </p>
        </div>
    </div>
    <div class="avatar m-auto">
        <div class="profile-pic w-75 m-auto">
            <img src="{{asset('img/avatar.png')}}" alt="..." class="w-75 h-auto">
        </div>
    </div>

</div>

@endsection
@extends('dashboard.master')
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Adding Member</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Members</a></li>
                    <li class="breadcrumb-item active">Add Member</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container d-flex flex-column p-5 border border-rounded bg-white w-50 ml-4">
    <div class="p-2 my-3">
        <h1 class="text-Secondary outline"> Adding A Member</h1>
    </div>
    <form id="add-member-form" action="{{route('members.store')}}" method="POST">
        @csrf
        <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Name:</label>
            <div class="col-sm-10">
                <input type="text" name="name" class="form-control" placeholder="Enter Name">
            </div>
        </div>
        <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Email:</label>
            <div class="col-sm-10">
                <input type="email" name="email" class="form-control" placeholder="Enter email">
            </div>
        </div>
        <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
                <input type="password" name="password" class="form-control" placeholder="Enter Password"
                    onmousedown="this.type='text'" onmouseup="this.type='password'" onmousemove="this.type='password'">
            </div>
        </div>
        {{-- <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Status</label>
            <div class="col-sm-10">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="admin-check">
                    <label class="form-check-label" for="admin-check">
                        Admin
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="member-check">
                    <label class="form-check-label" for="member-check">
                        Member
                    </label>
                </div>
            </div>
        </div> --}}
        <button type="submit" class="btn btn-primary float-right" style="width:6rem; height:3rem;"> <i
                class="fas fa-plus"></i>
            Add</button>
    </form>
</div>
@endsection
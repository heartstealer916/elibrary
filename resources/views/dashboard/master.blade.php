<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{asset('js/script.js')}}" defer></script>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="/" class="nav-link">Home</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item d-none d-sm-inline-block m-auto">
                    <div class="admin-icon mx-1">
                        <i class="fas fa-user-cog"></i>
                    </div>
                <li class="nav-item d-none d-sm-inline-block m-auto">
                    <div class="admin-name mr-2">
                        {{Auth::user()->name}}
                    </div>
                <li class="nav-item d-none d-sm-inline-block m-auto">
                    <div class="status">
                        <i class="fas fa-signal mx-3 text-success"><small class="mx-1">online</small></i>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                        <i class="fas fa-th-large"></i>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->


        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="/admin/dashboard" class="brand-link">
                <i class="fas fa-tachometer-alt"></i>
                <span class="brand-text font-weight-light">Dashboard</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="info">
                        <a href="/admin/dashboard" class="d-block"><i class="fas fa-user-check"></i>
                            {{Auth::user()->name}}</a>
                    </div>
                </div>

                <!-- SidebarSearch Form -->
                <div class="form-inline">
                    <div class="input-group" data-widget="sidebar-search">
                        <input class="form-control form-control-sidebar" type="search" placeholder="Search"
                            aria-label="Search">
                        <div class="input-group-append">
                            <button class="btn btn-sidebar">
                                <i class="fas fa-search fa-fw"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <li class="nav-item">
                            {{-- <li class="nav-item menu-open"> --}}
                            <a href="" class="nav-link">
                                <i class="fas fa-book"></i>
                                <p>
                                    Ebook
                                    <i class="right fas fa-chevron-circle-left"></i>
                                    {{-- <i class="right fas fa-angle-left"></i> --}}
                                </p>
                            </a>

                            <ul class="nav nav-treeview" style="background-color: rgb(34,45,50)">
                                <li class="nav-item">
                                    <a href="/ebooks" class="nav-link">
                                        <i class="fas fa-swatchbook"></i>
                                        <p>Ebooks</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="/ebooks/create" class="nav-link">
                                        <i class="fas fa-plus"></i>
                                        <p>Add eBook</p>
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="far fa-plus-square"></i>
                                        <p>Add Catogary</p>
                                    </a>
                                </li> --}}
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="fas fa-user-plus"></i>
                                <p>
                                    Member
                                    <i class="right fas fa-chevron-circle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview" style="background-color: rgb(34,45,50)">
                                <li class="nav-item">
                                    <a href="/members" class="nav-link">
                                        <i class="fas fa-users"></i>
                                        <p>Members</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="/members/create" class="nav-link">
                                        <i class="fas fa-plus"></i>
                                        <p>Add Member</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="fas fa-cog"></i>
                                <p>
                                    Settings
                                </p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->

            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                @include('dashboard.errors')
                @yield('content')
            </div>
            <!-- /.content-wrapper -->

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
                <div class="p-3">
                    <h5>Title</h5>
                    <p>Sidebar content</p>
                </div>
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <script src="{{asset('js/app.js')}}"></script>
</body>

</html>
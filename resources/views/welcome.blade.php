@extends('layouts.app')
@section('content')
<div class="ebooks-container w-75 d-inline m-auto">
    <h1>All Ebooks</h1>
    @foreach ($ebooks as $ebook)
    <div class="ebook-box m-1 p-2 bg-light shadow-lg">
        <div class="ebook-info d-flex flex-column w-25 d-inline-block">
            <label class="ebook-name"> {{$ebook->name}} </label>
            <label class="ebook-author">{{$ebook->author}}</label>
            <img src="{{ asset($ebook->pic) }}" alt="no image" class="ebook-cover w-25 h-25">
        </div>
    </div>
    @endforeach
</div>

@endsection
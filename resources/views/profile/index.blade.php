@extends('layouts.app')
@section('content')

<div class="container w-75 h-auto m-auto d-flex flex-row align-items-start">
    <div class="left w-25 d-flex flex-column bg-white">
        <div class="my-account border border-success d-flex flex-row py-2" style="background-color: #49b14d;">
            <svg class="mt-1 mx-2 text-white" xmlns="http://www.w3.org/2000/svg" width="25" height="25"
                fill="currentColor" class="bi bi-justify" viewBox="0 0 16 16">
                <path fill-rule="evenodd"
                    d="M2 12.5a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z" />
            </svg>
            <h2 style="color: white">My Account</h2>
        </div>
        <div class="my-profile border border-success d-flex flex-row py-2">
            <svg class="mt-1 mx-2" xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                class="bi bi-person-circle" viewBox="0 0 16 16" style="color: #49b14d">
                <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                <path fill-rule="evenodd"
                    d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
            </svg>
            <h4><a class="profile-btn" style="color: #49b14d">My Profile</a></h4>
        </div>
        <div class="my-orders border border-success d-flex flex-row py-2">
            <svg class="mt-1 mx-2" xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                class="bi bi-hexagon-fill" viewBox="0 0 16 16" style="color: #49b14d">
                <path fill-rule="evenodd"
                    d="M8.5.134a1 1 0 0 0-1 0l-6 3.577a1 1 0 0 0-.5.866v6.846a1 1 0 0 0 .5.866l6 3.577a1 1 0 0 0 1 0l6-3.577a1 1 0 0 0 .5-.866V4.577a1 1 0 0 0-.5-.866L8.5.134z" />
            </svg>
            <h4><a class="orders-btn" style="color: #49b14d">My Order</a></h4>
        </div>

    </div>
    <div class="right w-75 h-auto mx-2">
        <div class="profile-info">
            <div class="profile d-flex flex-column bg-white shadow-sm p-3 mb-5 bg-white rounded">
                <div class="profile-pic w-50 m-auto">
                    <img src="{{asset('img/avatar.png')}}" alt="..." class=" w-75 h-auto">
                </div>
                <div class="info w-50 pt-4">
                    <h4>Status:</h4><span>
                        @if ($info->is_admin == 1)
                        <h5>Admin</h5>
                        @else
                        <h5>Member</h5>
                        @endif

                    </span>
                    <h4 class="pt-2">Email:</h4><span>
                        <h5>{{$info->email}}</h5>
                    </span>
                    <h4 class="pt-2">Name:</h4><span>
                        <h5>{{$info->name}}</h5>
                    </span>
                    {{-- <h4 class="pt-2">Password:</h4><span>
                    <h5>{{decrypt($info->password)}}</h5>
                    </span> --}}

                </div>
            </div>
        </div>
        <div class="orders-info hidden ">
            <div class="orders d-flex bg-white flex-column shadow-sm p-3 mb-5 rounded pt-4">
                <p>Your orders placed here!!</p>
            </div>

        </div>

    </div>
</div>


@endsection
@extends('dashboard.master')
@section('content')
<style>
    .ebook-container {
        width: 100%;
        height: 100%;
    }
</style>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Read Ebook</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Ebooks</a></li>
                    <li class="breadcrumb-item active">View Ebook</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="ebook-container d-flex flex-column p-4 border border-rounded bg-white">
    <div class="name">
        <p class="fs-4"><strong>Name: </strong>{{$ebook->name}}</p>
    </div>
    <div class="author">
        <p><strong>Author: </strong>{{$ebook->author}}</p>
    </div>
    <div class="notes">
        <p><strong>Notes: </strong>{{$ebook->notes}}</p>
    </div>
    <div class="pdf">
        <iframe src="{{asset($ebook->pdf)}}" id="Iframe" width="100%" height="100%"></iframe>
    </div>
</div>
<script>
    var frame = document.getElementById("Iframe");
          frame.onload = function()
          {
            frame.style.height = 
            frame.contentWindow.document.body.scrollHeight + '100vh';
            frame.style.width  = 
            frame.contentWindow.document.body.scrollWidth+'px';
                
          }
</script>
@endsection
@extends('dashboard.master')
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Adding Ebook</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Ebooks</a></li>
                    <li class="breadcrumb-item active">Add Ebook</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container d-flex flex-column p-5 border border-rounded bg-white w-50 ml-4">
    <form id="add-ebook-form" action="{{route('ebooks.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
                <input type="text" name="name" class="form-control" placeholder="Enter Ebook Name"
                    value="{{old('name')}}">
            </div>
        </div>
        <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Author:</label>
            <div class="col-sm-10">
                <input type="text" name="author" class="form-control" placeholder="Enter Author Name"
                    value="{{old('author')}}">
            </div>
        </div>
        <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Notes</label>
            <div class="col-sm-10">
                <textarea name="notes" class="form-control" cols="30" rows="10" placeholder="Enter Notes"></textarea>
            </div>
        </div>
        <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Cover Photo</label>
            <div class="col-sm-10">
                <input type="file" class="form-control" name="pic" style="border:none; color:green; font-size:1.1rem;"
                    accept="image/*">
            </div>
        </div>
        <div class="row mb-3">
            <label class="col-sm-2 col-form-label"> Ebook File</label>
            <div class="col-sm-10">
                <input type="file" class="form-control" name="pdf" style="border:none; color:green; font-size:1.1rem;"
                    accept="application/pdf">
            </div>
        </div>
        <button type="submit" class="btn btn-primary float-right" style="width:6rem; height:3rem;"> <i
                class="fas fa-plus"></i>
            Add</button>
    </form>
</div>
@endsection
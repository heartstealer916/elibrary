@extends('dashboard.master')
@section('content')
<style>
	.ebook {
		position: relative;
		display: inline-block;
		width: 21.35%;
		height: auto;
		margin: 1rem;
	}

	.ebook-img {
		display: block;
		width: 100%;
		height: 33vh;
	}

	.overlay {
		position: absolute;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		height: 100%;
		width: 100%;
		opacity: 0;
		transition: .5s ease;
		background-color: #2e2e2e;
	}

	.ebook:hover .overlay {
		opacity: 0.9;
	}

	.overlay-content {
		padding: 1rem;
		width: 100%;
		height: 100%;
		display: flex;
		flex-direction: column;
		align-items: center;
		justify-content: center;
		font-size: 1rem;
	}

	.action-btns {
		display: flex;
		font-size: 1.5rem;
		flex-direction: row;
		align-items: center;
		justify-content: center;
	}

	.ebooks-rack {
		display: inline-block;
		width: 100%;
	}
</style>
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0">Ebooks</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Ebooks</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid d-flex flex-column p-1 border border-rounded bg-white">
	<div class="add-ebook mt-3 mx-3">
		<a href="{{ route('ebooks.create')}}">
			<button>
				<i class="fas fa-plus"></i>
				Add Ebook
			</button>
		</a>
	</div>
	<div class="ebooks-rack">
		@foreach ($ebooks as $ebook)
		<div class="ebook">
			<img src="{{asset($ebook->pic)}}" alt="Avatar" class="ebook-img">
			<div class="overlay">
				<div class="overlay-content">
					<p class="text-white"><strong> Name:</strong> {{$ebook->name}}</p>
					<p class="text-white"><strong> Author:</strong> {{$ebook->author}}</p>
					<p class="text-white"><strong> Notes:</strong> <small>{{$ebook->notes}}</small></p>
					<div class="action-btns">
						<a class="px-2" href="{{route('ebooks.show' , $ebook->id) }}"><button><i
									class="fas fa-eye"></i></button></a>

						<form class="delete-ebook-form" action="{{ route('ebooks.destroy',$ebook->id) }}" method='POST'>
							@csrf
							@method('DELETE')
							<button type="submit"><i class="far fa-trash-alt"></i></button>
						</form>
					</div>
				</div>
			</div>

		</div>
		@endforeach
	</div>
</div>

@endsection
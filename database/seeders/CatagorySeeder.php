<?php

namespace Database\Seeders;

use App\Models\Catagory;
use Illuminate\Database\Seeder;

class CatagorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $catagories = [
            [
                'name' => 'Science',
                'description' => 'All books related to science and technology are listed in that catagory',
            ],
            [
                'name' => 'Fiction',
                'description' => 'All books related to fiction and exajerations are listed in that catagory',
            ],
            [
                'name' => 'Computer Science',
                'description' => 'All books related to Coding and programming are listed in that catagory',
            ]

        ];

        foreach ($catagories as $catagory) 
        {
            Catagory::create($catagory);
        }
    }
    
}

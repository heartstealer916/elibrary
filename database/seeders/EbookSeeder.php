<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Ebook;

class EbookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ebooks = [
            [
                'name' => 'Try Try Hard',
                'author' => 'William Heed',
                'notes' => 'William heed is a very nic poet about many books he write with full enthuisiam',
                'pic' => 'covers/pic1.jpeg',
                'pdf' => 'docs/book1.pdf',
                'cat_id' => '1',
            ],
            [
                'name' => 'Hard',
                'author' => 'REhanna Heed',
                'notes' => 'REhanna Heed is a very nic poet about many books he write with full enthuisiam',
                'pic' => 'covers/pic2.jpeg',
                'pdf' => 'docs/book2.pdf',
                'cat_id' => '2',
            ],
            [
                'name' => 'ASsumpton',
                'author' => 'William Heed',
                'notes' => 'William heed is a very nic poet about many books he write with full enthuisiam',
                'pic' => 'covers/pic3.jpeg',
                'pdf' => 'docs/book3.pdf',
                'cat_id' => '3',
            ]

        ];

        foreach ($ebooks as $ebook) 
        {
            Ebook::create($ebook);
        }
    }
    
}
